import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class MainClass {

    public static void main(String[] args) {
//        Устанавливаем путь к драйверу, чтобы Idea могла найти exe файл драйвера
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\Nikita\\IdeaProjects\\testselenium\\drivers\\geckodriver.exe");

//        Инициализация драйвера
        WebDriver driver = new FirefoxDriver();

//        Установка неявного ожидания
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

//       Управление размером окна, использовать максимальный размер окна
//        driver.manage().window().maximize();

//       Управление размером окна, указываем необходимый размер окна
        driver.manage().window().setSize(new Dimension(900, 500));

//        Вызываем у драйвера метод get, позволяющий загрузить страницу
        driver.get("https://www.google.com");

//        Альтернативный метод запуска страницы
        driver.navigate().to("https://www.seleniumhq.org/");

//        Возвращаемся на предыдущую страницу
        driver.navigate().back();

//        Возвращаемся на страницу вперёд
        driver.navigate().forward();

//        Метод, позволяющий перезагрузить страницу
        driver.navigate().refresh();

//        Метод для получения title
        System.out.println(driver.getTitle());

//        Метод для получения ссылки страницы
        System.out.println(driver.getCurrentUrl());

//        Закрытие браузера и завершение работы драйвера
        driver.quit();


    }
}
